package com.parallel.future;

import java.util.concurrent.Callable;

public class MicroTask implements Callable<String>{
  public static int threadId = 0;
  
  private int instance = 0;
  
  private int from;
  private int to;
  
  public MicroTask(int from, int to){
    super();
    this.from = from;
    this.to = to;
    instance = ++threadId;
  }
  
  public String call() throws Exception {
    int count = 0;
    for(int i = from; i<= to; i++){
      for(int j = 2; j < i/2; j++){
        if( i%j == 0 )
          count++;
      }
    }
    
    for(int i = from; i<= to; i++){
      for(int j = 2; j < i/2; j++){
        if( i%j == 0 )
          count++;
      }
    }
    
    for(int i = from; i<= to; i++){
      for(int j = 2; j < i/2; j++){
        if( i%j == 0 )
          count++;
      }
    }
    
    for(int i = from; i<= to; i++){
      for(int j = 2; j < i/2; j++){
        if( i%j == 0 )
          count++;
      }
    }
    
    for(int i = from; i<= to; i++){
      for(int j = 2; j < i/2; j++){
        if( i%j == 0 )
          count++;
      }
    }
    return "ThreadID: " + instance + " ["+count+"]";
  }

}
