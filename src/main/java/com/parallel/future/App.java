package com.parallel.future;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;

/**
 * Hello world!
 *
 */
public class App{
    public enum CORE{
      SINGLE, HALF, FULL;
    }
    
    public static void main( String[] args ) throws InterruptedException, ExecutionException{
      int availableCores = Runtime.getRuntime().availableProcessors();
      CORE core = CORE.HALF;
      int useCores = availableCores;
      switch(core){
      case FULL:
        useCores = availableCores;
        break;
      case HALF:
        useCores = availableCores/2;
        break;
      case SINGLE:
        useCores = 1;
        break;
      default:
        break;
      }
      
      App.burryyyyWithCores(useCores, Integer.MAX_VALUE);
    }
    
    public static void burryyyyWithCores(int core, int number) throws InterruptedException, ExecutionException{
      String result = " ";
      long start = System.currentTimeMillis();
      ExecutorService taskExecutor = Executors.newFixedThreadPool(core);
      List<FutureTask<String>> futureTaskList = new ArrayList<FutureTask<String>>();
      
      for(int t = 0; t<core; t++){
        FutureTask<String> futureTask = new FutureTask<String>(new MicroTask(1, number/core));
        futureTaskList.add(futureTask);
        taskExecutor.execute(futureTask);
      }
      
      for (int j = 0; j < core; j++) {
        FutureTask<String> futureTask = futureTaskList.get(j);
        result += futureTask.get();
      }
        
      taskExecutor.shutdown();
      
      System.out.println(result);
      long timeTaken = System.currentTimeMillis() - start;
      System.err.println("\nTime Taken: " + timeTaken + " ms");
    }
}
